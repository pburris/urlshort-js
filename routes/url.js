var express = require('express'),
    router = express.Router(),
    Url = require('../models/urls.js'),
    validUrl = require('valid-url');

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for( var i=0; i < 3; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

router.get('/', function(req, res, next) {
  res.send('<h3>API base');
});

router.get(/^\/(.+)/, function(req, res) {
  var href = req.params[0];
  console.log(href.slice(0, 2).toLowerCase());
  if (href.slice(0, 4).toLowerCase() === 'www.') {
    href = 'http://' + href;
    console.log(href);
  }
  if (validUrl.isWebUri(href)){
    var newUrl = new Url({url: href, shortUrl: makeid()});
    newUrl.save(function(err, data) {
      if (err) res.json(err)
        res.jsonp({url: href, shortUrl: 'http://' + req.get('host') + '/' + data.shortUrl});
    });
  } else {
    res.jsonp({err: "Invalid URL"});
  }
});

module.exports = router;
