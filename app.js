var express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    Url = require('./models/urls.js'),
    api = require('./routes/url.js');

mongoose.connect(process.env.MONGODB_URL);
const PORT = process.env.PORT || 3000;

app.use('/api', api);

app.get('/', function(req, res){
  res.send('<h2>URL Shortener</h2> <p>Use: /api/:url here and the shortened url is at /shortcode/:url </p><p>Please include http://');
});

app.get('/:url', function(req, res) {
  var url = req.params.url;
  Url.findOne({shortUrl: url}, function(err, data) {
    if (err) res.json(err);

    if (data === null){
      res.json({err: "ShortUrl not found"});
    } else {
      res.redirect(data.url);
    }
  });
});

console.log('App listening on port: ' + PORT);
app.listen(PORT);
