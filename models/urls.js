var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var urlSchema = new Schema({
  url: String,
  shortUrl: String
});

var Url = mongoose.model('url', urlSchema);

module.exports = Url;
